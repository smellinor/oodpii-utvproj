package se.ellinor.utvproj.game.model;

import java.util.List;

import se.ellinor.utvproj.game.model.players.MoveablePlayer;
import se.ellinor.utvproj.game.model.players.factory.MoveablePlayerFactory;
import se.ellinor.utvproj.game.model.players.factory.MoveablePlayerFactoryIface;
import se.ellinor.utvproj.game.model.players.shape.Point;
import se.ellinor.utvproj.game.view.Frame;

import java.util.ArrayList;

public class GameFacade {

	private List<MoveablePlayer> asteroids;
	private List<MoveablePlayer> bullets;
	private List<MoveablePlayer> enemyBullets;
	private List<MoveablePlayer> enemyShips;
	private MoveablePlayer ship;
	private double shipSpeed;
	private int score;
	private MoveablePlayerFactoryIface playerFactory;

	public GameFacade() {
		this.reset();
	}
	
	public List<MoveablePlayer> getPlayers() {
		List<MoveablePlayer> players = new ArrayList<>();
		players.addAll(this.asteroids);
		players.addAll(this.bullets);
		players.addAll(this.enemyBullets);
		players.addAll(this.enemyShips);
		players.add(this.ship);
		return players;
	}
	
	public int getScore() {
		return this.score;
	}

	public void aimSpaceShip(double x, double y) {
		this.ship.aim(new Point(x,y));
	}

	public void setSpaceShipSpeed(double x, double y) {
		this.shipSpeed = Math.sqrt(Math.pow(x - this.ship.getTip().getX(), 2) + Math.pow(y-this.ship.getTip().getY(), 2));
	}

	public void moveSpaceShip() {
		for(int i = 0; i< this.shipSpeed/20. ; i++)
			this.ship.move();
	}

	public boolean move() {
		this.moveSpaceShip();
		this.move(this.asteroids);
		this.move(this.bullets);
		this.move(this.enemyBullets);
		this.move(this.enemyShips);
		this.hits(this.bullets, this.asteroids);

		this.deleteablePlayers(this.asteroids);
		this.deleteablePlayers(this.bullets);
		this.deleteablePlayers(this.enemyBullets);
		this.deleteablePlayers(this.enemyShips);
		
		if(this.collision(this.ship, this.enemyBullets))
			return true;
		if(this.collision(this.ship, this.asteroids))
			return true;
		
		if(this.ship.getTip().getX() >= Frame.SIZE)
			this.ship.move(-Frame.SIZE, 0);
		if(this.ship.getTip().getY() >= Frame.SIZE)
			this.ship.move(0., -Frame.SIZE);
		
		return false;
	}

	private boolean collision(MoveablePlayer s, List<MoveablePlayer> list) {
		for(MoveablePlayer a : list) {
			if(s.on(a)) {
				return true;
			}
		}
		return false;
	}

	private void hits(List<MoveablePlayer> list1, List<MoveablePlayer> list2) {
		List<MoveablePlayer> remove1 = new ArrayList<>();
		List<MoveablePlayer> remove2 = new ArrayList<>();
		for(int i = 0 ; i < list1.size() ; i++) {
			for(int j = 0; j < list2.size() ; j++) {
				MoveablePlayer a = list1.get(i);
				MoveablePlayer b = list2.get(j);
				if(a.on(b)) {
					remove1.add(a);
					remove2.add(b);
					this.score++;
				}
			}
		}
		list1.removeAll(remove1);
		list2.removeAll(remove2);
	}

	private void deleteablePlayers(List<MoveablePlayer> list) {
		for(int i = 0 ; i < list.size() ; i++) {
			MoveablePlayer p = list.get(i);
			if(p.getTip().getX() >= Frame.SIZE || p.getTip().getY() >= Frame.SIZE) {
				list.remove(i);
			}
		}
	}

	private void move(List<MoveablePlayer> list) {
		for(MoveablePlayer p : list)
			p.move();
	}

	public void shoot() {
		MoveablePlayer b = this.playerFactory.createBullet();
		b.move(this.ship.getTip().getX(), this.ship.getTip().getY());
		b.aim(new Point(this.ship.getTip().getX()*Frame.SIZE, this.ship.getTip().getY()*Frame.SIZE));
		this.bullets.add(b);
	}

	public void enemyShoot() {
		for(MoveablePlayer p : this.enemyShips) {
			MoveablePlayer b = this.playerFactory.createBullet();
			b.move(p.getTip().getX(), p.getTip().getY());
			b.aim(new Point(Math.random()*500., Math.random()*500.));
			this.enemyBullets.add(b);
		}
	}

	public void addAsteroid() {
		MoveablePlayer a = this.playerFactory.createAsteroid();
		a.aim(new Point(Frame.SIZE + Math.random()*Frame.SIZE, Frame.SIZE + Math.random()*Frame.SIZE));
		asteroids.add(a);
	}

	public void addEnemySpaceShip() {
		MoveablePlayer s = this.playerFactory.createSpaceShip();
		s.move(-Frame.SIZE*Math.random()/2., Math.random()*Frame.SIZE/2.);
		s.aim(new Point(Frame.SIZE + Math.random()*Frame.SIZE, Frame.SIZE + Math.random()*Frame.SIZE));
		this.enemyShips.add(s);
	}
	
	public void reset() {
		this.asteroids = new ArrayList<>();
		this.bullets = new ArrayList<>();
		this.enemyBullets = new ArrayList<>();
		this.enemyShips = new ArrayList<>();
		this.playerFactory = new MoveablePlayerFactory();
		this.ship = this.playerFactory.createSpaceShip();
		this.score = 0;
	}

}
