package se.ellinor.utvproj.game.model.players;

import java.awt.Graphics;

import se.ellinor.utvproj.game.model.players.shape.Line;
import se.ellinor.utvproj.game.model.players.shape.Point;

public interface MoveablePlayer {
	
	public void move(double dx, double dy);
	public void move();
	public void aim(Point p);
	public boolean on(MoveablePlayer p);
	public boolean on(Line p);
	public boolean on(Point p);
	public Point getTip();
	public void draw(Graphics g);
}
