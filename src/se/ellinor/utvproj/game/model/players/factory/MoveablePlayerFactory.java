package se.ellinor.utvproj.game.model.players.factory;

import se.ellinor.utvproj.game.model.players.Asteroid;
import se.ellinor.utvproj.game.model.players.Bullet;
import se.ellinor.utvproj.game.model.players.MoveablePlayer;
import se.ellinor.utvproj.game.model.players.SpaceShip;

public class MoveablePlayerFactory implements MoveablePlayerFactoryIface {
	
	@Override
	public MoveablePlayer createBullet() {
		return new Bullet();
	}
	
	@Override
	public MoveablePlayer createAsteroid() {
		return new Asteroid();
	}
	
	@Override
	public MoveablePlayer createSpaceShip() {
		return new SpaceShip();
	}

}
