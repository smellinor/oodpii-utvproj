package se.ellinor.utvproj.game.model.players.factory;

import se.ellinor.utvproj.game.model.players.MoveablePlayer;

public interface MoveablePlayerFactoryIface {

	public MoveablePlayer createAsteroid();

	public MoveablePlayer createBullet();

	public MoveablePlayer createSpaceShip();

}
