package se.ellinor.utvproj.game.model.players;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import se.ellinor.utvproj.game.model.players.shape.Line;
import se.ellinor.utvproj.game.model.players.shape.Point;
import se.ellinor.utvproj.game.view.Frame;

public class SpaceShip implements MoveablePlayer {

	private List<Line> lines;

	public SpaceShip() {
		this.lines = new ArrayList<>();
		this.lines.add(new Line(8., 0., 8., 20.));
		this.move(Frame.SIZE/2., Frame.SIZE/2.);
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.white);
		for(Line l : this.lines)
			l.draw(g);
	}

	@Override
	public Point getTip() {
		return new Point(this.lines.get(0).getStart());
	}

	@Override
	public void move(double dx, double dy) {
		for(Line l : this.lines)
			l.move(dx, dy);
	}

	@Override
	public void move() { // move length of 1 in current direction
		for(Line l : this.lines) {
			double length = l.getLength();
			Line norm = new Line(new Point(0., 0.), Point.minus(l.getStart(),l.getEnd()));
			norm.getEnd().setX(norm.getEnd().getX()/length);
			norm.getEnd().setY(norm.getEnd().getY()/length);
			l.move(norm.getEnd().getX(), norm.getEnd().getY());
		}
	}

	@Override
	public void aim(Point p) {
		for(Line l : this.lines)
			l.aim(p);
	}

	@Override
	public boolean on(Point p) {
		for(Line l : this.lines) {
			if(l.on(p)) {
				return true;
			} 
		}
		return false;
	}

	@Override
	public boolean on(MoveablePlayer p) {
		for(Line l : this.lines) {
			if(p.on(l)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean on(Line p) {
		for(Line l : this.lines) {
			if(l.on(p)) {
				return true;
			}
		}
		return false;
	}

}
