package se.ellinor.utvproj.game.model.players.shape;

public interface Shape {
	
	public void move(double dx, double dy);
	public void moveTo(Point p);

}
