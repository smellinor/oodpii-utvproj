package se.ellinor.utvproj.game.model.players.shape;

public class Point implements Shape {

	private double x;
	private double y;

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Point(Point p) {
		this.x = p.getX();
		this.y = p.getY();
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	@Override
	public void moveTo(Point p) {
		this.x = p.getX();
		this.y = p.getY();
	}

	@Override
	public void move(double dx, double dy) {
		this.x += dx;
		this.y += dy;
	}

	public static Point minus(Point p1, Point p2) {
		return new Point(p1.getX()-p2.getX(), p1.getY()-p2.getY());
	}
	
	public static Point plus(Point p1, Point p2) {
		return new Point(p1.getX()+p2.getX(), p1.getY()+p2.getY());
	}

	@Override
	public boolean equals(Object p) {
		if(p instanceof Point) {
			Point p1 = (Point) p;
			return Double.compare(p1.getX(), this.getX()) == 0. && Double.compare(p1.getY(), this.getY()) == 0.;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		return String.format("(%.1f, %.1f)", this.x, this.y);
	}

}
