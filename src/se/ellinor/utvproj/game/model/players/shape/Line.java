package se.ellinor.utvproj.game.model.players.shape;

import java.awt.Graphics;

public class Line implements Shape {

	private Point end;
	private Point start;

	public Line(Point start, Point end) {
		this.end = end;
		this.start = start;
	}

	public Line(double x1, double y1, double x2, double y2) {
		this.start = new Point(x1, y1);
		this.end = new Point(x2, y2);
	}
	
	public Line(Line l) {
		this.start = new Point(l.getStart());
		this.end = new Point(l.getEnd());
	}

	@Override
	public void move(double dx, double dy) {
		this.end.move(dx, dy);
		this.start.move(dx, dy);
	}
	
	@Override
	public void moveTo(Point p) {
		this.end.moveTo(Point.minus(p, this.end));
		this.start.moveTo(p);
	}

	public Point getEnd() {
		return end;
	}

	public void setEnd(Point end) {
		this.end = end;
	}

	public Point getStart() {
		return this.start;
	}

	public void setStart(Point start) {
		this.start = start;
	}

	public double getLength() {
		return Math.sqrt(Math.pow(this.start.getX() - this.end.getX(), 2.) + Math.pow(this.start.getY() - this.end.getY(), 2.));
	}

	public void draw(Graphics g) {
		g.drawLine(
				(int) this.start.getX(), (int) this.start.getY(), 
				(int) this.end.getX(), (int) this.end.getY());
	}

	public void aim(Point p) { //rotates
		//moves start so line is parallel with p-end
		p = Point.minus(p, this.start);
		double l = new Line(new Point(0., 0.), p).getLength();
		p = new Point(p.getX()/l, p.getY()/l); // normalized direction vector
		double length = this.getLength();
		this.start = Point.plus(this.end, new Point(p.getX()*length, p.getY()*length));
	}

	@Override
	public String toString() {
		return this.start.toString() + ", " + this.end.toString();
	}

	public boolean on(Point p) {
		return new Line(p, this.getStart()).getLength() + new Line(p, this.getEnd()).getLength() == this.getLength();
	}

	private boolean ccw(Point a, Point b,Point c) {
		return (c.getY() - a.getY())*(b.getX() - a.getX()) > (b.getY() - a.getY())*(c.getX() - a.getX());
	}

	public boolean on(Line l) {
		return ccw(this.getStart(),l.getStart(),l.getEnd()) != ccw(this.getEnd(),l.getStart(),l.getEnd()) && ccw(this.getStart(),this.getEnd(),l.getStart()) != ccw(this.getStart(),this.getEnd(),l.getEnd());
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Line) {
			Line l = (Line) o;
			return (this.getEnd().equals(l.getEnd()) && this.getStart().equals(l.getStart())) || (this.getEnd().equals(l.getStart()) && this.getStart().equals(l.getEnd()));
		}
		return false;
	}
}
