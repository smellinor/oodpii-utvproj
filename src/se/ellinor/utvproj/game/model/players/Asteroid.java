package se.ellinor.utvproj.game.model.players;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import se.ellinor.utvproj.game.model.players.shape.Line;
import se.ellinor.utvproj.game.model.players.shape.Point;
import se.ellinor.utvproj.game.view.Frame;

public class Asteroid implements MoveablePlayer {

	private List<Line> lines;
	private Point direction;
	private static final double MAX_LENGTH = Frame.SIZE/10.;
	private static final double MIN_LENGTH = 3.;

	public Asteroid() {
		int nrOfLines = 3 + (int)(Math.random()*4.);
		this.lines = new ArrayList<>();
		lines.add(new Line(
				new Point(MIN_LENGTH + Math.random()*(MAX_LENGTH-MIN_LENGTH), 
						MIN_LENGTH + Math.random()*(MAX_LENGTH-MIN_LENGTH)),
				new Point(MIN_LENGTH + Math.random()*(MAX_LENGTH-MIN_LENGTH), 
						MIN_LENGTH + Math.random()*(MAX_LENGTH-MIN_LENGTH))));
		for(int i = 1; i < nrOfLines-1; i++) {
			lines.add(new Line(
					lines.get(i-1).getEnd(),
					new Point(MIN_LENGTH + Math.random()*(MAX_LENGTH-MIN_LENGTH), 
							MIN_LENGTH + Math.random()*(MAX_LENGTH-MIN_LENGTH))));
		}
		lines.add(new Line(
				lines.get(lines.size()-1).getEnd(),
				lines.get(0).getStart()));
		this.direction = new Point(0., 0.);

	}

	@Override
	public Point getTip() {
		return new Point(this.lines.get(0).getStart());
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.white);
		for(Line l : this.lines)
			l.draw(g);
	}

	@Override
	public void move(double dx, double dy) {
		for(Line l : this.lines) 
			l.move(dx, dy);
	}

	@Override
	public boolean on(Point p) {
		for(Line l : this.lines)
			if(l.on(p))
				return true;
		return false;
	}

	@Override
	public void move() {
		Line dir2Point = new Line(new Point(0., 0.), Point.minus(direction, this.getTip()));
		double length = dir2Point.getLength();
		dir2Point.getEnd().setX(dir2Point.getEnd().getX()/length);
		dir2Point.getEnd().setY(dir2Point.getEnd().getY()/length);
		this.move(dir2Point.getEnd().getX(), dir2Point.getEnd().getY());
	}

	@Override
	public boolean on(MoveablePlayer p) {
		for(Line l : this.lines) {
			if(p.on(l)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean on(Line p) {
		for(Line l : this.lines) {
			if(l.on(p)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void aim(Point p) {
		this.direction = new Point(p);
	}

}
