package se.ellinor.utvproj.game.model.players;

import java.awt.Color;
import java.awt.Graphics;

import se.ellinor.utvproj.game.model.players.shape.Line;
import se.ellinor.utvproj.game.model.players.shape.Point;

public class Bullet implements MoveablePlayer {

	private Line l;

	public Bullet(Point p1, Point p2) { //starts at p1, length 1 in direction of p2
		this.l = new Line(p1, p2);
	}
	
	public Bullet()  {
		this.l = new Line(0., 0., 0., 0.);
	}
	
	public void setPoint(Point p) {
		this.l.setStart(p);
	}

	@Override
	public Point getTip() {
		return new Point(this.l.getStart());
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.white);
		g.drawOval((int)this.l.getStart().getX(), (int)this.l.getStart().getY(), 1, 1);
	}

	@Override
	public void move(double dx, double dy) {
		this.l.move(dx, dy);
	}

	@Override
	public void move() { // move length of 1 in current direction
		Line dir2Point = new Line(new Point(0., 0.), Point.minus(this.l.getEnd(), this.l.getStart()));
		double length = dir2Point.getLength();
		dir2Point.getEnd().setX(dir2Point.getEnd().getX()/length);
		dir2Point.getEnd().setY(dir2Point.getEnd().getY()/length);
		this.move(dir2Point.getEnd().getX(), dir2Point.getEnd().getY());
	}

	@Override
	public boolean on(Point p) {
		return p.equals(this.l.getStart());
	}

	@Override
	public boolean on(MoveablePlayer p) {
		return p.on(l);
	}

	@Override
	public boolean on(Line p) {
		return p.on(l);
	}

	@Override
	public void aim(Point p) {
		this.l.setEnd(p);
	}

}
