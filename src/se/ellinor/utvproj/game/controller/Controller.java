package se.ellinor.utvproj.game.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import se.ellinor.utvproj.game.model.GameFacade;
import se.ellinor.utvproj.game.view.Frame;
import se.ellinor.utvproj.game.view.Frame.Mode;

public class Controller {

	private Frame main;
	private GameFacade game;
	private int loops;

	public Controller() {
		this.main = new Frame(this);
		this.game = new GameFacade();
	}

	public void game() {
		Timer t = new Timer(50, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(main.getMode().equals(Mode.GAME)) {
					if(game.move()) {
						main.setMode(Mode.ENDGAME);
						game.reset();
						((Timer)e.getSource()).stop();
					}

					if(loops%200 == 0) {
						game.addEnemySpaceShip();
					}
					if(loops%30 == 0) {
						game.addAsteroid();
						game.enemyShoot();
					}
					main.update(game.getPlayers(), game.getScore());
					loops++;
					main.repaint();
				}
			}});
		t.start();
	}

	public void mouseClicked() {
		if(this.main.getMode().equals(Mode.GAME)) {
			this.game.shoot();
			this.main.repaint();
		}
	}

	public void mouseMoved(double x, double y) {
		if(this.main.getMode().equals(Mode.GAME)) {
			this.game.aimSpaceShip(x, y);
			this.game.setSpaceShipSpeed(x, y);
			this.main.repaint();
		}
	}

	public static void main(String[] args) {
		new Controller();
	}

}
