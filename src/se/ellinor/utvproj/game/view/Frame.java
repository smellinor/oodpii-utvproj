package se.ellinor.utvproj.game.view;

import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;

import se.ellinor.utvproj.game.controller.Controller;
import se.ellinor.utvproj.game.model.players.MoveablePlayer;
import se.ellinor.utvproj.game.view.state.BaseState;
import se.ellinor.utvproj.game.view.state.factory.StateFactory;

public class Frame extends JFrame {

	public enum Mode {
		MENU, GAME, ENDGAME, EXIT;

		public static Mode convert(String mode) {
			if(mode.equalsIgnoreCase("GAME")) {
				return GAME;
			} else if(mode.equalsIgnoreCase("MENU")) {
				return MENU;
			} else if(mode.equalsIgnoreCase("ENDGAME")) {
				return ENDGAME;
			} else if(mode.equalsIgnoreCase("EXIT")) {
				return EXIT;
			} else {
				return null;
			}
		}
	}

	private static final long serialVersionUID = 1L;
	public static final int SIZE = 500;
	private BaseState currentState;
	private Controller controller;
	private MouseMotionListener mouse;
	private List<MoveablePlayer> players;
	private int score;

	public Frame(Controller controller) {
		super("ASTEROIDS");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(SIZE, SIZE);
		this.setVisible(true);
		this.setCurrentState(new StateFactory(this).createMenuState());
		this.controller = controller;
		this.mouse = new MouseMotionListener(this);
	}

	public Mode getMode() {
		return Mode.convert(this.currentState.toString());
	}
	
	public void click() {
		this.controller.mouseClicked();
	}
	
	public void moved(double x, double y) {
		this.controller.mouseMoved(x, y);
	}

	public void setMode(Mode mode) {
		BaseState state;
		switch(mode) {
		case MENU:
			state = new StateFactory(this).createMenuState();
			state.addMouseListener(this.mouse);
			state.addMouseMotionListener(this.mouse);
			this.setCurrentState(state);
			break;
		case GAME:
			state = new StateFactory(this).createGameState();
			state.addMouseListener(this.mouse);
			state.addMouseMotionListener(this.mouse);
			this.setCurrentState(state);
			this.controller.game();
			break;
		case ENDGAME:
			state = new StateFactory(this).createEndGameState();
			state.addMouseListener(this.mouse);
			state.addMouseMotionListener(this.mouse);
			this.setCurrentState(state);
			break;
		case EXIT:
			this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
			break;
		default:
			break;
		}
	}

	private void setCurrentState(BaseState state) {
		if(this.currentState != null)
			this.remove(this.currentState);
		this.currentState = state;
		this.add(this.currentState);
		this.revalidate();
		this.repaint();
	}
	
	public void update(List<MoveablePlayer> list, int score) {
		this.players = list;
		this.score = score;
	}
	
	public List<MoveablePlayer> getPlayers() {
		return this.players;
	}
	
	public int getScore() {
		return this.score;
	}
}
