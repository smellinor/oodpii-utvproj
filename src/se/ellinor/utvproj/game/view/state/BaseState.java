package se.ellinor.utvproj.game.view.state;

import java.awt.Color;

import javax.swing.JPanel;

import se.ellinor.utvproj.game.view.Frame;

public abstract class BaseState extends JPanel {

	private static final long serialVersionUID = 1L;
	protected Frame frame;
	
	public BaseState(Frame frame) {
		super();
		this.setBackground(Color.black);
		this.frame = frame;
	}
	
}
