package se.ellinor.utvproj.game.view.state.button;

import java.awt.Graphics;
import java.awt.event.MouseEvent;

import se.ellinor.utvproj.game.view.Frame;
import se.ellinor.utvproj.game.view.Frame.Mode;

public class EndGameButton extends Button {

	public EndGameButton(Frame main) {
		super("", main);
	}

	private static final long serialVersionUID = 1L;

	@Override
	public void mouseReleased(MouseEvent e) {
		main.setMode(Mode.ENDGAME);
	}

	@Override
	public void paintComponent(Graphics g) {
		
	}
}
