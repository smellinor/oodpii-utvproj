package se.ellinor.utvproj.game.view.state.button.factory;

import se.ellinor.utvproj.game.view.state.button.Button;

public interface ButtonFactoryIface {
	
	public Button createMenuButton();
	public Button createGameButton();
	public Button createExitButton();

}
