package se.ellinor.utvproj.game.view.state.button.factory;

import se.ellinor.utvproj.game.view.Frame;
import se.ellinor.utvproj.game.view.state.button.Button;
import se.ellinor.utvproj.game.view.state.button.ExitButton;
import se.ellinor.utvproj.game.view.state.button.GameButton;
import se.ellinor.utvproj.game.view.state.button.MenuButton;

public class ButtonFactory implements ButtonFactoryIface {
	
	protected Frame main;

	public ButtonFactory(Frame main) {
		this.main = main;
	}
		
	@Override
	public Button createGameButton() {
		return new GameButton(this.main);
	}
	
	@Override
	public Button createMenuButton() {
		return new MenuButton(this.main);
	}
	
	@Override
	public Button createExitButton() {
		return new ExitButton(this.main);
	}
	
	

}
