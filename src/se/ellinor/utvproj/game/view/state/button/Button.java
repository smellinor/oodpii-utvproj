package se.ellinor.utvproj.game.view.state.button;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

import se.ellinor.utvproj.game.view.Frame;

public abstract class Button extends JComponent implements MouseListener {

	private static final long serialVersionUID = 1L;
	private String label;
	private Color labelColor;
	protected Frame main;

	protected static final int HEIGHT = Frame.SIZE/6;
	protected static final int WIDTH = Frame.SIZE/3;

	public Button(String text, Frame main) {
		this.label = text;
		this.addMouseListener(this);
		this.labelColor = Color.white;
		this.main = main;
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(this.labelColor);
		g.setFont(g.getFont().deriveFont(25.f));
		g.drawString(this.label, WIDTH + 20, this.getLocation().y + 50);
		g.setColor(Color.white);
		g.drawRect(WIDTH, this.getLocation().y, WIDTH, HEIGHT);
	}

}
