package se.ellinor.utvproj.game.view.state.button;

import java.awt.event.MouseEvent;

import se.ellinor.utvproj.game.view.Frame;
import se.ellinor.utvproj.game.view.Frame.Mode;

public class MenuButton extends Button {

	private static final long serialVersionUID = 1L;

	public MenuButton(Frame main) {
		super("    MENU", main);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getX() > WIDTH && e.getX() < WIDTH*2) {
			if(e.getY() > 0 && e.getY() < HEIGHT) {
				main.setMode(Mode.MENU);
			}
		}
	}


}
