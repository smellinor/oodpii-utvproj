package se.ellinor.utvproj.game.view.state.button;

import java.awt.event.MouseEvent;

import se.ellinor.utvproj.game.view.Frame;
import se.ellinor.utvproj.game.view.Frame.Mode;

public class GameButton extends Button {

	private static final long serialVersionUID = 1L;

	public GameButton(Frame main) {
		super("NEW GAME", main);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getX() > WIDTH && e.getX() < WIDTH*2) {
			if(e.getY() > 0 && e.getY() < HEIGHT) {
				main.setMode(Mode.GAME);
			}
		}
	}

}
