package se.ellinor.utvproj.game.view.state;

import java.awt.Graphics;
import java.awt.GridLayout;

import se.ellinor.utvproj.game.view.state.button.Button;
import se.ellinor.utvproj.game.view.state.button.factory.ButtonFactory;

public class EndGameState extends BaseState {

	private static final long serialVersionUID = 1L;
	
	private Button gameButton;
	private Button exitButton;
	private Button menuButton;
	
	public EndGameState(ButtonFactory buttonFactory) {
		super(null);
		this.gameButton = buttonFactory.createGameButton();
		this.menuButton = buttonFactory.createMenuButton();
		this.exitButton = buttonFactory.createExitButton();
		this.setLayout(new GridLayout(3,1));
		this.add(this.gameButton);
		this.add(this.menuButton);
		this.add(this.exitButton);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.gameButton.paint(g);
		this.menuButton.paint(g);
		this.exitButton.paint(g);
	}
	
	@Override
	public String toString() {
		return "ENDGAME";
	}

}
