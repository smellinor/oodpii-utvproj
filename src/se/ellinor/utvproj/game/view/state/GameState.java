package se.ellinor.utvproj.game.view.state;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;

import se.ellinor.utvproj.game.model.players.MoveablePlayer;
import se.ellinor.utvproj.game.view.Frame;

public class GameState extends BaseState {

	private static final long serialVersionUID = 1L;

	public GameState(Frame frame) {
		super(frame);
	}
	
	@Override
	public String toString() {
		return "GAME";
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.white);
		g.drawString("" + this.frame.getScore(), 3, 13);
		List<MoveablePlayer> players = this.frame.getPlayers();
		if(players != null && !players.isEmpty())
			for(MoveablePlayer p : players)
				p.draw(g);
	}

}
