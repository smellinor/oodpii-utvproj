package se.ellinor.utvproj.game.view.state;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JComponent;

import se.ellinor.utvproj.game.view.Frame;
import se.ellinor.utvproj.game.view.state.button.Button;
import se.ellinor.utvproj.game.view.state.button.factory.ButtonFactory;

public class MenuState extends BaseState {
	
	private static final long serialVersionUID = 1L;
	private Button exitButton;
	private Button gameButton;

	public MenuState(ButtonFactory buttonFactory) {
		super(null);
		this.gameButton = buttonFactory.createGameButton();
		this.exitButton = buttonFactory.createExitButton();
		this.setLayout(new GridLayout(3,1));
		this.add(new TitleLabel("ASTEROIDS"));
		this.add(this.gameButton);
		this.add(this.exitButton);
	}
	
	@Override
	public String toString() {
		return "MENU";
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.gameButton.paint(g);
		this.exitButton.paint(g);
	}
	
	private class TitleLabel extends JComponent {

		private static final long serialVersionUID = 1L;
		private String text;
		
		public TitleLabel(String text) {
			this.text = text;
		}
		
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(Color.WHITE);
			g.setFont(g.getFont().deriveFont(30.f));
			g.drawString(this.text, (int)(Frame.SIZE/3)+5, this.getLocation().y + 70);
		}
		
	}

}
