package se.ellinor.utvproj.game.view.state.factory;

import se.ellinor.utvproj.game.view.Frame;
import se.ellinor.utvproj.game.view.state.BaseState;
import se.ellinor.utvproj.game.view.state.EndGameState;
import se.ellinor.utvproj.game.view.state.GameState;
import se.ellinor.utvproj.game.view.state.MenuState;
import se.ellinor.utvproj.game.view.state.button.factory.ButtonFactory;

public class StateFactory implements StateFactoryIface {
	
	protected Frame main;
	
	public StateFactory(Frame main) {
		this.main = main;
	}
	
	@Override
	public BaseState createMenuState() {
		return new MenuState(new ButtonFactory(this.main));
	}
	
	@Override
	public BaseState createGameState() {
		return new GameState(this.main);
	}
	
	@Override
	public BaseState createEndGameState() {
		return new EndGameState(new ButtonFactory(this.main));
	}

}
