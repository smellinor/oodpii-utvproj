package se.ellinor.utvproj.game.view.state.factory;

import se.ellinor.utvproj.game.view.state.BaseState;

public interface StateFactoryIface {
	
	public BaseState createMenuState();
	public BaseState createGameState();
	public BaseState createEndGameState();

}
