package se.ellinor.utvproj.game.view;

import java.awt.event.MouseEvent;

import javax.swing.event.MouseInputAdapter;

public class MouseMotionListener extends MouseInputAdapter {
	
	private Frame frame;

	public MouseMotionListener(Frame frame) {
		this.frame = frame;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		this.frame.click();
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		this.frame.moved(e.getX(), e.getY());
	}
	
}
